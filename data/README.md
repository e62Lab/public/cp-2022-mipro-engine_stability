# Raw data

Raw results from simulations. All results are in **H5** files. Usually such files are large and thus not added to the repository. In this case, however, the size of the files is quite small so we decided to add them for the sake of completeness.
