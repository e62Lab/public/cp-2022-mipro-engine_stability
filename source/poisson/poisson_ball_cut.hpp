#ifndef POISSON_BALL_CUT_HPP
#define POISSON_BALL_CUT_HPP

#include <Eigen/Sparse>

#include "../poisson/poisson_case.hpp"
#include "../boilerplate/case_boilerplate.hpp"

using namespace mm;
using namespace std;
using namespace Eigen;

template <typename vec_t>
struct PoissonBallCut : public CaseBoilerplate<vec_t> {
    using Base = CaseBoilerplate<vec_t>;

    template <typename fill_t>
    static Eigen::VectorXd solve(const XML& conf, fill_t&& h, DomainDiscretization<vec_t>& d,
                                 HDF& file, Timer& timer) {
        // Domain interior and boundary.
        auto interior = d.interior();
        auto boundary = d.boundary();

        // Domain size.
        int N = d.size();

        // Create storage and compute stencils and weights.
        auto storage = Base::template computeStencilsAndWeights<sh::lap>(conf, d, &timer);
        SparseMatrix<double, RowMajor> M(N, N);
        Eigen::VectorXd rhs = Eigen::VectorXd::Zero(N);

        // Operators.
        auto op = storage.implicitOperators(M, rhs);

        // Interior.
        for (int i : interior) {
            op.lap(i) = u_lap(d.pos(i));
        }

        // Dirichlet boundary.
        for (int i : boundary) {
            auto pos = d.pos(i);

            op.value(i) = u_ana(pos);
        }

        if (conf.get<int>("debug.print") == 1) {
            cout << "Problem set, calling solver ..." << endl;
        }
        VectorXd sol = Base::sparseSolve(conf, M, rhs, &timer);

        return sol;
    }
};

#endif
