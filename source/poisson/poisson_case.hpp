#ifndef POISSON_CASE_FUNCTION_HPP
#define POISSON_CASE_FUNCTION_HPP

#include <medusa/Medusa_fwd.hpp>

using namespace std;
using namespace Eigen;

// Contains Poisson problem definition.

/**
 * @brief Analytic solution.
 *
 * @tparam vec_t Vector.
 * @param p Position.
 * @return double Return value.
 */
template <typename vec_t>
double u_ana(const vec_t& p) {
    double sin_product = 1.0;
    for (auto coordinate_value : p) {
        sin_product *= std::sin(mm::PI * coordinate_value);
    }

    return sin_product;
}

/**
 * @brief Analytic laplace of Poisson.
 *
 * @tparam vec_t Vector.
 * @param p Poisiton.
 * @return double Return value.
 */
template <typename vec_t>
double u_lap(const vec_t& p) {
    double sinProduct = u_ana(p);

    return -vec_t::dim * mm::PI * mm::PI * sinProduct;
}

#endif
