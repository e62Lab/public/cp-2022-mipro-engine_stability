#ifndef XML_HELPER_HPP
#define XML_HELPER_HPP

#include <medusa/Medusa_fwd.hpp>
#include "math_helper.hpp"

using namespace mm;
using namespace std;

Range<XML> GetXMLRange(const XML& conf) {
    if (conf.get<int>("debug.print") == 1) {
        cout << "Creating XML range ..." << endl;
    }

    // Initialize list of output XMLS.
    Range<XML> all_conf;

    Range<string> nodes = split(conf.get<string>("discretization.nodes_per_side"), ',');
    Range<string> basis_types = split(conf.get<string>("approx.basis_type"), ',');
    Range<string> mon_degrees = split(conf.get<string>("approx.mon_degree"), ',');
    Range<string> support_size_factors = split(conf.get<string>("approx.support_size_factor"), ',');
    auto N_runs = conf.get<int>("case.n_runs");
    auto dim = conf.get<int>("domain.dim");
    auto simulation_type = conf.get<string>("simulation.type");

    for (string node : nodes) {
        // Copy master configuration.
        XML _conf(conf);
        _conf.set("discretization.nodes_per_side", node, true);

        for (string basis_type : basis_types) {
            _conf.set("approx.basis_type", basis_type, true);

            for (string mon_degree : mon_degrees) {
                _conf.set("approx.mon_degree", mon_degree, true);

                Range<int> support_sizes;
                if (simulation_type == "support_scan") {
                    int min_n = std::ceil(stod(support_size_factors[0])* binomialCoeff(stoi(mon_degree) + dim, dim));
                    int max_n = std::ceil(stod(support_size_factors[1]) * binomialCoeff(stoi(mon_degrees[mon_degrees.size() - 1]) + dim, dim));
                    support_sizes = Range<int>::seq(min_n, max_n, (max_n - min_n) / 10);
                }
                else {
                    support_sizes.push_back(2 * binomialCoeff(stoi(mon_degree) + dim, dim));
                }
                
                for (int support_size : support_sizes) {
                    _conf.set("approx.support_size", format("%d", support_size), true);

                    for (int i = 0; i < N_runs; i++) {
                        // Store to range.
                        all_conf.push_back(_conf);
                    }
                }
            }
        }
    }

    if (conf.get<int>("debug.print") == 1) {
        cout << "XML range created." << endl;
    }

    return all_conf;
}

#endif  // XML_HELPER_HPP