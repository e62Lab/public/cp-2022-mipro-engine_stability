#include <medusa/Medusa_fwd.hpp>

#include "helpers/xml_helper.hpp"
#include "poisson/poisson_ball_cut.hpp"

using namespace std;
using namespace Eigen;
using namespace mm;

bool written = false;

template <typename vec_t, typename func_t>
void runSolve(const XML& conf, HDF& hdf, const BallShape<vec_t>& shape, const func_t& h, int i,
              Eigen::VectorXd& Ns, Eigen::VectorXd& times, Eigen::VectorXd& err1,
              Eigen::VectorXd& err2, Eigen::VectorXd& errinf) {
    if (conf.get<int>("debug.print") == 1) {
        cout << "Solving case " << i << " ..." << endl;
    }
    // Initialize timer.
    Timer t;
    t.addCheckPoint("start");

    // Domain discretization.
    auto interior_type = conf.get<int>("domain.interior");
    auto boundary_type = conf.get<int>("domain.boundary");

    // Fill domain with nodes.
    DomainDiscretization<vec_t> d = shape.discretizeBoundaryWithDensity(h, boundary_type);
    GeneralFill<vec_t> fill_randomized;
    fill_randomized.numSamples(conf.get<int>("discretization.num_samples")).seed(get_seed());
    d.addInternalNode(0.0, interior_type);
    fill_randomized(d, h, interior_type);

    int N = d.size();
    Ns(i) = N;
    if (conf.get<int>("debug.print") == 1) {
        cout << "Domain size: " << N << "." << endl;
    }

    VectorXd u = PoissonBallCut<vec_t>::solve(conf, h, d, hdf, t);

    if (!written) {
        written = true;
        // Add domain to output.
        hdf.atomic().writeDomain("domain", d);
        // Add solution to output.
        hdf.atomic().writeDoubleArray("domain/sol", u);
    }

    t.addCheckPoint("end");

    // Error evaulation.
    Eigen::VectorXd u_analytic(N);
    for (int k = 0; k < N; ++k) {
        u_analytic[k] = u_ana(d.pos(k));
    }
    VectorXd err = u_analytic - u;

    err1(i) = err.lpNorm<1>() / u_analytic.lpNorm<1>();
    err2(i) = err.lpNorm<2>() / u_analytic.lpNorm<2>();
    errinf(i) = err.lpNorm<Eigen::Infinity>() / u_analytic.lpNorm<Eigen::Infinity>();
    times(i) = t.duration("start", "end");

    if (conf.get<int>("debug.print") == 1) {
        cout << "Finished solving case " << i << " ..." << endl;
    }
}

template <typename vec_t>
void runAll(const XML& conf, HDF& hdf) {
    // Get range of XMLs.
    auto inputs = GetXMLRange(conf);
    auto N_inputs = inputs.size();

    // Initialize vectors.
    Eigen::VectorXd NS(N_inputs);
    Eigen::VectorXd TIMES(N_inputs);
    Eigen::VectorXd ERR1(N_inputs);
    Eigen::VectorXd ERR2(N_inputs);
    Eigen::VectorXd ERRI(N_inputs);
    Range<string> configurations(N_inputs);

#pragma omp parallel for
    for (int i = 0; i < N_inputs; i++) {
        try {
            // Current configuration.
            auto input = inputs[i];
            if (input.get<int>("debug.print") == 1) {
                prn(input);
            }

            auto dx = 1.0 / input.get<double>("discretization.nodes_per_side");
            auto fn = [=](const vec_t& p) { return dx; };

            // Run solve.
            auto origin = input.get<double>("domain.origin");
            auto radius = input.get<double>("domain.radius");
            BallShape<vec_t> ball(origin, radius);

            runSolve(input, hdf, ball, fn, i, NS, TIMES, ERR1, ERR2, ERRI);

            // Add configuration to array.
            auto basis_type = input.get<string>("approx.basis_type");
            auto mon_degree = input.get<int>("approx.mon_degree");
            auto nodes_per_side = input.get<int>("discretization.nodes_per_side");
            auto support_size = input.get<int>("approx.support_size");
            stringstream configuration;
            configuration << "basis=" << basis_type << ",m=" << mon_degree << ",nps=" << nodes_per_side
                        << ",idx=" << i << ",support=" << support_size;
            configurations[i] = configuration.str();
        }
        catch (const std::exception& e) // caught by reference to base
        {
            std::cout << " a standard exception was caught, with message '" << e.what() << "'\n";
        }
    }

    // Store to output.
    cout << "Results computed, saving to H5." << endl;
    hdf.reopen();
    hdf.openGroup("/");
    hdf.writeEigen("Ns", NS);
    hdf.writeEigen("err1", ERR1);
    hdf.writeEigen("err2", ERR2);
    hdf.writeEigen("errinf", ERRI);
    hdf.writeEigen("times", TIMES);
    for (int i = 0; i < N_inputs; i++) {
        hdf.writeStringAttribute(to_string(i), configurations[i]);
    }
    hdf.close();
}

int main(int argc, const char* argv[]) {
    // Check for settings hdf.
    assert_msg(argc >= 2, "Second argument should be the XML parameter hdf.");

    // Timer initialization.
    Timer t;
    t.addCheckPoint("start");

    // Read input config.
    cout << "Reading params from: " << argv[1] << endl;
    const XML conf(argv[1]);

    // Create H5 hdf to store the parameters.
    string output_hdf =
        conf.get<string>("meta.out_dir") + conf.get<string>("meta.out_file") + ".h5";
    cout << "Creating results hdf: " << output_hdf << endl;
    HDF hdf(output_hdf, HDF::DESTROY);

    // Write params to results hdf.
    hdf.writeXML("conf", conf);
    hdf.close();

    cout << "----------" << endl;
    cout << "Computing, please wait ..." << endl;
    int dim = conf.get<int>("domain.dim");
    switch (dim) {
        case 1:
            runAll<Vec1d>(conf, hdf);
            break;
        case 2:
            runAll<Vec2d>(conf, hdf);
            break;
        case 3:
            runAll<Vec3d>(conf, hdf);
            break;
        default:
            assert_msg(false, "Dimension %d not supported.", dim);
    }

    // End execution.
    t.addCheckPoint("end");
    prn(t.duration("start", "end"));
    cout << "Calculations saved to: " << output_hdf << "." << endl;

    return 0;
}
