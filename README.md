# Mipro 2022: Stability analysis of RBF-FD and WLS based local strong form meshless methods on scattered nodes

Repo for [mipro 2022 conference](http://www.mipro.hr/Default.aspx).

## Description / Abstract

The popularity of local meshless methods in the field of numerical simulations has increased greatly
in recent years. This is mainly due to the fact that they can operate on scattered nodes and that
they allow a direct control over the approximation order and basis functions. In this paper we
analyse two popular variants of local strong form meshless methods, namely the radial basis
function-generated finite differences (RBF-FD) using polyharmonic splines (PHS) augmented with
monomials, and the weighted least squares (WLS) approach using only monomials - a method also known
as diffuse approximation method. Our analysis focuses on the accuracy and stability of the numerical
solution computed on scattered nodes in a two- and three-dimensional domain. We show that the RBF-FD
variant exhibits a more stable behavior and a higher accuracy of the numerical solution for higher
order approximations, but at the cost of higher computational complexity, while the WLS variant is
more appropriate when lower order approximations are sufficient.

## Visuals

For progress and certain visuals please check the `results/` directory. In some cases, it might also
be beneficial to check the `logs/`.

## Installation

Requirements:

- CMake (2.8.12 or higher)
- C++ compiler
- Python 3.8 (or higher)
- Jupyter notebooks
- [Medusa](https://gitlab.com/e62Lab/medusa)

## Usage

Create or go to `build/` directroy and build using

```bash
cmake .. && make -j 12
```

The `fill_trumpet.sh` executable will be created in `bin/` directory.

## Support

Thanks to [E62 team](https://e6.ijs.si/parlab/) for support.

## Contributing

Entire [E62 team](https://e6.ijs.si/parlab/).

## Authors and contributors

- **Mitja Jančič** under supervision of
- **Gregor Kosec**

## Acknowledgments

The authors would like to acknowledge the financial support of the ARRS research core funding No.
P2-0095, ARRS project funding No. J2-3048 and the World Federation of Scientists.

# Reproducing paper results

To reproduce the paper results, compile and move to `bin/` directory to run

```bash
./fill_trumpet ../input/support_scan_dim_2.xml
```

to reproduce the support scan results and

```bash
./fill_trumpet ../input/trumpet_dim_2.xml
```

to reproduce the convergence rates.
