# Log

## 24. 01. 2022

- Created project.
- Added files.

## 25. 01. 2022

- Added plot script.
- Configured project.
- Check miniature results.
- Added run script.
- Prepared all for results.

## 15. 02. 2022

- First paper draft.
- Removed 1D analysis as scattered nodes in 1D make no sense at all.

## 22. 02. 2022

- Added support size scan to the results.
- Modified code to compute support size scan.
- Paper updated.

## 04. 03. 2022

- Paper submitted.

# To-do

- Introduction
