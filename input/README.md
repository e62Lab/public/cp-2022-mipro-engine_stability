# Input files

Usually out code has input configuration files written in `.json` or `.xml`. Such configuration files should be stored here.

## Details

Optional settings:

- `basis_type`: "phs" or "mon".
- `solver_type`: "pardiso" or "sparselu".
  > For `pardiso` you will have to uncomment some lines but generally speaking it should work.
- `dim`: "1", "2" or "3".